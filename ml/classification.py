# -*- coding: utf-8 -*-
"""
Created on Sun May 27 17:07:11 2012
Test des différentes méthodes de classification supervisées

@author: alban Thomas
"""
from __future__ import print_function

import sys
import os
import logging

import numpy as np
import pandas as pd
import pylab as pl
from sklearn.cross_validation import StratifiedKFold, cross_val_score
from sklearn import grid_search
from sklearn.metrics import classification_report #precision_score
from sklearn import neighbors, linear_model, svm, tree
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier
#from sklearn.manifold.isomap import Isomap
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import SGDClassifier
from sklearn.semi_supervised import LabelPropagation, LabelSpreading
from sklearn.linear_model import Perceptron, PassiveAggressiveClassifier
from sklearn.feature_selection import RFECV

from maplearn.ml.machine import Machine
from maplearn.ml.confusion import Confusion
from maplearn.ml.distance import Distance
from maplearn.app.reporting import str_extend

logger = logging.getLogger(__name__)


def lcs_kernel(x, y):
    """
    Kernel custom basé sur DTW
    """
    dist = Distance(x, y)
    return dist.run(meth='lcs')

def svm_kernel(x, y):
    """
    Kernel custom basé sur DTW
    """
    dist = Distance(x, y)
    return dist.run(meth='dtw')

# LISTE DES ALGOS EN STOCK
TUNE_ALGO = dict( \
            knn={'n_neighbors':[3, 5, 10, 20],
                 'weights':['uniform', 'distance'],
                 'algorithm':['auto', 'ball_tree', 'kd_tree'],
                 'leaf_size':[15, 30, 50]},
            log={'penalty':['l1', 'l2'], 'C':[0.1, 1, 10, 100]},
            svm=[{'kernel': ['rbf'], 'gamma': np.logspace(-6, -1, 6), \
                'C': [0.1, 1, 10, 100, 1000]},
                 {'kernel': ['linear', 'poly', 'sigmoid'], \
                 'C': [0.1, 1, 10, 100, 1000]}],
            nearestc={'shrink_threshold':[None, 0.1, 0.2, 0.5]},
            lda=[{'solver':['lsqr', 'eigen'],
                  'shrinkage':[None, 0.1, 0.2, 0.5, 'auto']},
                 {'solver':['svd'], 'tol':[None, 0.1, 0.2, 0.5]}])

class Classification(Machine):
    """
    Applique des classifications à un jeu de données
    - avec des échantillons associées à des données
    - d'autres données
    """
    __SCORING = "precision_weighted"
    def __init__(self, data, algorithms=None, **kwargs):
        """
        Initialisation
        """
        Machine.ALL_ALGOS = dict( \
                knn=neighbors.KNeighborsClassifier(),
                log=linear_model.LogisticRegression(),
                tree=tree.DecisionTreeClassifier(),
                gnb=GaussianNB(),
                mnb=MultinomialNB(),
                lda=LinearDiscriminantAnalysis(),
                nearestc=neighbors.NearestCentroid(),
                svm=svm.SVC(kernel='rbf'),
                svc=svm.SVC(kernel='linear'),
                gboost=GradientBoostingClassifier(),
                bag=BaggingClassifier(),
                rforest=RandomForestClassifier(),
                extra=ExtraTreesClassifier(),
                sgd=SGDClassifier(),
                propag=LabelPropagation(),
                spreading=LabelSpreading(),
                ada=AdaBoostClassifier(),
                perceptron=Perceptron(),
                passive=PassiveAggressiveClassifier(),)
        # Paramètre de la Cross-Validation
        self.kfold = 3 if 'kfold' not in kwargs else kwargs['kfold']
        self.__dct_scores = dict()
        Machine.__init__(self, algorithms=algorithms, data=data, **kwargs)


        self.algorithms = algorithms

        if 'metric' in kwargs:
            if kwargs['metric'] == 'dtw':
                dist = Distance()
                metric = dist.dtw
                kernel = svm_kernel
            elif kwargs['metric'] == 'lcs':
                dist = Distance()
                metric = dist.lcs
                kernel = lcs_kernel
            else:
                metric = kwargs['metric']
                #TODO : avoir un kernel propre a chaque distance
                kernel = 'linear'
            for clf in self._algorithms:
                if 'metric' in self._algorithms[clf].get_params():
                    self._algorithms[clf].set_params(metric=metric)
                elif 'kernel' in self._algorithms[clf].get_params():
                    self._algorithms[clf].set_params(kernel=kernel)

    def load(self, data):
        """ Charge les données nécessaires à la classification d'un site et
        d'une année
        """
        Machine.load(self, data)
        if self._data.Y is None:
            raise AttributeError('Aucun échantillon spécifié')
        if self._data.data is None:
            self._data.data = self._data.X

        # analyse de l'echantillon en fonction de la cross-validation
        frq = self._data.classes
        if min(frq.values()) < self.kfold:
            print("""WARNING : les classes suivantes ne contiennent pas
                  assez d'echantillons :""")
            for k, v  in frq:
                i = 0
                if v < self.kfold:
                    self._data.X = self._data.X[self._data.Y != k, :]
                    self._data.Y = self._data.Y[self._data.Y != k]
                    sys.stdout.write("%i (%i) | " %(k, v))
                    i += 1
            print("\n=> %i classes seront ignorees" % i)
            logger.warning('%i classes ignored due to too few members', i)
        return 0

    def predict_1(self, algo, proba=False):
        """
        Applique la classification à toute la matrice de données
        """
        Machine.predict_1(self, algo)
        if proba:
            try:
                self.clf.set_params(probability=True)
            except ValueError:
                logger.warning('Probabilities unavailable with %s', algo)
                proba = False
        # classification sur tous les donnees
        self.clf.fit(self._data.X, self._data.Y)
        logger.info('Algorithm %s trained on whole dataset', algo)
        result = self.clf.predict(self._data.data).astype(np.int8)
        logger.info('Prediction on whole dataset with %s', algo)
        # 1er resultat ou ajout d'une classification a d'autres resultats
        # existants
        if self.result is None:
            self.result = pd.DataFrame(data=result, columns=[algo,],
                                       dtype=np.int8)
        else:
            self.result[algo] = result
        if proba:
            # recupere la proba max (celle de la classe retenue)
            try:
                self.result[algo+'_pr'] = \
                    np.round(np.max(self.clf.predict_proba( \
                             self._data.data).astype(np.float), 1), 5)
            except (NotImplementedError, AttributeError):
                logger.warning('Probabilities unavailable with %s', algo)
            else:
                logger.info('Probabilities using %s calculated', algo)

    def fit_1(self, algo):
        """
        Mesure la qualité d'1 classification (par cross-validation)
        La cross-validation est ensuite appliquée
        """
        Machine.fit_1(self, algo)
        # 1. CLASSIFICATION par k-fold
        print('\n####%s - Entrainement classification####' % algo)
        skf = StratifiedKFold(y=self._data.Y, n_folds=self.kfold)
        i = 0
        k = np.zeros(shape=self.kfold)
        for t, v in skf:
            print('\n#####%s (%i/%i)#####' % (algo, i+1, self.kfold))
            # entrainement sur un k-fold
            try:
                self.clf.fit(self._data.X[t, :], self._data.Y[t])
            except ValueError as e:
                logger.error('%s ne peut être appliqué', algo)
                logger.error(e.message)
                return None
            except (AttributeError, TypeError):
                logger.error('ERREUR dans classification %s => Exclusion',
                             algo)
                #self._algorithms.pop(name) # exclusion de la classification
                return None

            # prediction sur le reste
            result_clf = self.clf.predict(self._data.X[v, :])
            print(classification_report(self._data.Y[v], result_clf))
            mat_confusion = Confusion(self._data.Y[v], result_clf)
            mat_confusion.calcul_matrice()
            str_file = os.path.join(self.dir_out,
                                    '_'.join((algo, str(i), 'cm')))
            mat_confusion.export(fTxt=str_file+'.txt', fPlot=str_file+'.png')
            k[i] = mat_confusion.kappa
            i += 1

        # STATSTIQUES de la Classification (k-fold)
        try:
            scores = cross_val_score(self.clf, self._data.X, self._data.Y, \
                                     cv=self.kfold, scoring=self.__SCORING)
            recalls = cross_val_score(self.clf, self._data.X, self._data.Y, \
                                     cv=self.kfold, scoring='recall_weighted')
            f1s = cross_val_score(self.clf, self._data.X, self._data.Y, \
                         cv=self.kfold, scoring='f1_weighted')
        except ValueError:
            logger.warning('%s - Score de cross-validation incalculable',
                           algo)
        else:
            self._fitted = True
            print('\n**%s - Cross-Validation %i-fold :**' %(algo, self.kfold))
            print("* Accuracy: %.2f (+/-%.2f)" %(scores.mean(), scores.std()))
            print("* Kappa : %.2f (+/-%.2f)\n" % (np.mean(k), np.std(k)))
            self.__dct_scores[algo] = [ \
                "%.2f (+/-%.2f)" %(scores.mean(), scores.std()), \
                "%.2f (+/-%.2f)" % (recalls.mean(), recalls.std()), \
                "%.2f (+/-%.2f)" % (f1s.mean(), f1s.std()), \
                "%.2f (+/-%.2f)" % (np.mean(k), np.std(k))]

    def export_tree(self, out_file=None):
        """
        Export d'un arbre de décision
        """
        if out_file is None or out_file == '':
            out_file = os.path.join(self.dir_out, 'decision_tree.dot')
        clf = tree.DecisionTreeClassifier()
        clf.fit(X=self._data.X, y=self._data.Y)
        with open(out_file, 'w') as file_:
            file_ = tree.export_graphviz(clf, out_file=file_)
        logger.info("Decision tree exported")

    def run(self, predict=False):
        """
        Application de la classificaion suivant toutes les les méthodes
        demandées
        """
        Machine.run(self, predict)
        print('(Cross-Validation %i-fold)\n' %self.kfold)
        size = 20

        str_msg = '|'+'|'.join([str_extend(txt, size) for txt in \
                  ['Classifier', 'Precision', 'Recall', 'F1', 'Kappa']])
        str_msg += '|\n'
        str_msg += '|'+'|'.join(['-'*size,]*5)
        str_msg += '|\n'

        for i in sorted(self.__dct_scores):
            str_msg += '|'+'|'.join([str_extend(txt) for txt in \
                  [i, self.__dct_scores[i][0], self.__dct_scores[i][1], \
                  self.__dct_scores[i][2], self.__dct_scores[i][3]]])
            str_msg += '|\n'
        print(str_msg)
        lst_exclu = list(set(self.algorithms) ^ set(self.__dct_scores.keys()))
        if len(lst_exclu) > 0:
            print("\n+ %i classifications avec erreurs : %s" \
                  % (len(lst_exclu), ','.join(lst_exclu)))

    def rfe(self, algo):
        """
        Recursive Feature Elimination
        Selection des features en fonction d'un classifier
        """
        if algo is not None:
            self.clf = self._algorithms[algo]
        if self.clf is None:
            algo = self.algorithms[0]
            self.clf = self._algorithms[algo]
        # Create the RFE object and compute a cross-validated score.
        rfecv = RFECV(estimator=self.clf, step=1,
                      cv=StratifiedKFold(self._data.Y, self.kfold),
                      scoring='accuracy')
        try:
            rfecv.fit(self._data.X, self._data.Y)
        except ValueError:
            logger.warning("La selection des features n'est pas possible avec\
                le classifier %s", self._algorithms[algo])
            return 1
        print("Optimal number of features : %d" % rfecv.n_features_)

        # Plot number of features VS. cross-validation scores
        pl.figure()
        pl.xlabel("Number of features selected")
        pl.ylabel("Cross validation score (nb of misclassifications)")
        pl.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
        pl.show()

        # affichage textuel des features retenus
        feat = rfecv.get_support(True)
        # application de la sélection des features
        self._data.X = self._data.X[:, feat]

        feat = [str(i) for i in feat]
        print('%i Features retenus : %s' % (len(feat), ' | '.join(feat)))

    def optimize(self, algo):
        """
        Optimisation des paramètres d'un classifieur
        """

        if algo not in TUNE_ALGO:
            logger.warning('Aucune optimisation disponible pour %s', algo)
            return None

        # normalisation des donnees => accelere GridSearchCV
        if np.mean(self._data.X) < 0.9 or np.mean(self._data.X) > 1.1:
            self._data.scale()
        score_name = self.__SCORING.split('_')[0]
        print("####Optimisation de %s (but : best %s)####\n"
              % (algo, score_name))
        gs = grid_search.GridSearchCV(self._algorithms[algo], \
             param_grid=TUNE_ALGO[algo], \
             n_jobs=4, cv=self.kfold, scoring=self.__SCORING)

        gs.fit(self._data.X, self._data.Y)
        y_true, y_pred = self._data.Y, gs.predict(self._data.X)

        print("\n * Meilleurs parametres : " + str(gs.best_params_))
        print("\n * Meilleur estimateur: " + str(gs.best_estimator_))
        print("\n * Meilleur score : %.3f \n" % gs.best_score_)
        print(classification_report(y_true, y_pred))

        self._algorithms[algo+'.optim'] = gs.best_estimator_

