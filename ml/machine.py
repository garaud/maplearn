# -*- coding: utf-8 -*-
"""

Created on Fri Mar 11 12:03:25 2016

@author: thomas_a
"""
from __future__ import print_function
import os
import logging
import numpy as np

logger = logging.getLogger('maplearn.'+__name__)


class Machine(object):
    """
    Machine permettant d'appliquer un ou plusieurs algorithmes de classification
    (supervisée ou non)
    """
    ALL_ALGOS = dict()
    #Parametres lies a performance
    # Cache size = taille en RAM dispo (en Mo)
    # N_JOBS : nombre de processeurs à utiliser (-1 = tous)
    _params = {'cache_size':24000000, 'n_jobs':-1}
    def __init__(self, data, algorithms=None, **kwargs):
        self._algorithms = dict() # algorithmes a appliquer
        self.clf = None # algorithme en cours d'utilisation
        self._data = None # jeux de données sur lequel travailler
        self._fitted = False
        self._na = np.nan
        self.result = None  # resultat des predictions des classifs

        # dossier en sortie
        if 'dirOut' not in kwargs or kwargs['dirOut'] is None:
            self.dir_out = os.getcwd()
            logger.warning('Dossier en sortie non spécifié => %s utilisé',
                           self.dir_out)
        else:
            self.dir_out = kwargs['dirOut']
        if not os.path.exists(self.dir_out):
            os.makedirs(self.dir_out)
            logger.info('Dossier %s inexistant => créé', self.dir_out)

        # chargement des données
        if data is not None:
            self.load(data)
        if 'na' in kwargs:
            self._na = kwargs['na']

    @property
    def algorithms(self):
        """
        Renvoie la liste des algorithmes qui seront utilises
        """
        return [i for i in self._algorithms.keys()]

    @algorithms.setter
    def algorithms(self, algorithms):
        """
        Sélectionne les algorithmes à utiliser parmi tous les algorithmes
        existants
        """
        if Machine.ALL_ALGOS is None or len(Machine.ALL_ALGOS) < 1:
            raise KeyError('Aucun algorithme disponible')
        if isinstance(algorithms, str):
            algorithms = [algorithms,]
        if algorithms is None or len(algorithms) == 0:
            self._algorithms = Machine.ALL_ALGOS
            self.__set_params()
            return None
        try:
            self._algorithms = {k:Machine.ALL_ALGOS[k] for k in algorithms}
        except KeyError:
            str_msg = 'Algorithm(s) unknown in :'
            str_msg += ','.join(algorithms)
            logger.critical(str_msg)
            raise KeyError(str_msg)
        logger.info('%i algorithms set', len(self._algorithms))
        self.__set_params()

    def __set_params(self):
        """
        Applique les paramètres de performances aux classifieurs
        """
        for i in self.algorithms:
            params = {k:Machine._params[k] for k in Machine._params.keys() \
                      if k in self._algorithms[i].get_params()}
            if len(params) > 0:
                logger.debug('"%i" parameters to change in %s', len(params), i)
                self._algorithms[i].set_params(**params)
        logger.debug('Performance parameters applied')

    def predict_1(self, algo, export=False):
        """
        Prediction des données à l'aide d'un algorithme
        """
        self.__apply_1(algo)
        if export and self.dir_out is None:
            logger.error('EXPORT impossible : dossier en sortie absent')
            export = False

        if not self._fitted:
            logger.warning("Algorithme %s n'a pas ete entraine", algo)
            self.fit_1(algo)
            if not self._fitted:
                logger.error("Algorithme %s n'a pas pu etre entraine", algo)
                return None
        logger.info('Prédiction par %s', algo)

    def fit_1(self, algo):
        """
        Entrainement d'un algorithme au jeu de données en cours
        """
        self._fitted = False
        self.__apply_1(algo)
        logger.info("Training algorithm %s", algo)

    def __apply_1(self, algo):
        """
        Application d'un algorithme
        """
        if algo not in self._algorithms.keys():
            raise Exception('Unknown algorithm : %s' % algo)
        self.clf = self._algorithms[algo]

    def load(self, data):
        """ Charge les données nécessaires à la classification d'un site et
        d'une année
        """
        logger.debug('Loading data for processing...')
        """
        # TODO comment tester qu'il s'agit bien d'un packdata ?
        if not isinstance(data, PackData):
            logger.critical('Waiting a Packdata')
        """
        if hasattr(data, 'X') and hasattr(data, 'Y') and hasattr(data, 'data'):
            self._data = data
        else:
            raise TypeError('Assuming a packdata to process')
        logger.info('Data loaded for processing')

    def run(self, predict=False):
        """
        Application de la classificaion suivant toutes les les méthodes
        demandées
        """
        print(self)
        for name in self._algorithms.iterkeys():
            self.fit_1(name)
            if self._fitted and predict:
                self.predict_1(name)
        print('\n###Comparaison de(s) %i classifier(s)###'
              %len(self._algorithms))

    def __str__(self):
        msg = '\n### Liste de(s) algorithme(s) ###'
        for k in self._algorithms:
            msg += '\n* **%s** : *%s*' %(k, self._algorithms[k])
        msg += '\n'
        return msg
