# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 21:48:26 2016

@author: thomas_a
"""
import unittest
import os
import numpy as np

from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
from maplearn.test import DIR_TMP

class TestPackData(unittest.TestCase):
    """ Tests unitaires concernant les jeux de données
    """
    def setUp(self):
        
        loader = Loader('iris')
        
        for f in os.listdir(DIR_TMP):
            os.remove(os.path.join(DIR_TMP, f))
        self.__data = PackData(X=loader.X, Y=loader.Y, data=loader.aData)

    def test_empty_pack(self):
        """
        Création d'un packdata vide (x, y et data == None)
        """
        self.assertRaises(ValueError, PackData)

    def test_xy_wrong_dim(self):
        """
        Chargement de X et Y avec des dimensions différentes
        """
        y = np.random.randint(1, 10, size=10)
        x = np.random.random((5, 5))
        self.assertRaises(IndexError, PackData, x, y)

    def test_xy_data_wrong_dim(self):
        """
        Chargement d'un jeu de données complet (X, Y, data) où data
        a des dimensions incompatibles avec X
        """
        y = np.random.randint(1, 10, size=10)
        x = np.random.random((10, 5))
        data = np.random.random((10, 6))
        self.assertRaises(IndexError, PackData, x, y, data)
    
    def test_balance(self):
        """
        Rééquilibrage d'un échantillon déséquilibré
        """
        y = np.concatenate((np.ones(40, dtype=np.int),
                           np.random.randint(2, 10, size=10)))
        x = np.random.random((50, 5))
        pckdata = PackData(X=x, Y=y)
        pckdata.balance()
        print(pckdata.classes)
        self.assertLess(pckdata.Y.shape[0], 50)
        
    def test_scale(self):
        """
        Mise à l'échelle
        """
        try:
            self.__data.scale()
        except:
            self.fail("Scaling raised an exception")
    
    def test_balance_few_classes(self):
        """
        Rééquilibrage d'un échantillon déséquilibré avec que 2 classes
        """
        y = np.concatenate((np.ones(40, dtype=np.int),
                           np.random.randint(2, 3, size=10)))
        x = np.random.random((50, 5))
        pckdata = PackData(X=x, Y=y)
        pckdata.balance()
        print(pckdata.classes)
        self.assertLess(pckdata.Y.shape[0], 50)

if __name__ == '__main__':
    unittest.main()