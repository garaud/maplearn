# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 22:00:54 2015

@author: thomas_a
"""
from __future__ import print_function

import os
import unittest
import numpy as np
from test import DIR_DATA, DIR_TMP

from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.filehandler.shapefile import Shapefile
from maplearn.app.benchmark import Benchmark


class TestBenchmark(unittest.TestCase):
    """ Tests unitaires de la classe Clustering (classif non supervisée)
    """
    def setUp(self):
        src_data = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        src_samples = os.path.join(DIR_DATA, 'samples_landsat_rennes.tif')
        self.benImg = Benchmark(DIR_TMP, type='classification', 
                                algorithm='knn')
        self.benImg.load(src_samples)
        self.benImg.load_data(src_data)

    def test_load_image(self):
        """
        Test de chargement des échantillons et des données à partir d'images
        """
        
        self.assertGreater(self.benImg.dataset.X.shape[0], 0)
        self.assertEqual(self.benImg.dataset.X.shape[0], 
                         self.benImg.dataset.Y.shape[0])
    
    def test_classify_image(self):
        """
        Test d'application d'une classification à une image
        """
        out_file = os.path.join(DIR_TMP, 'export.tif')
        self.benImg.training()
        self.benImg.run(True)
        self.assertTrue(os.path.exists(out_file))
        img = ImageGeo(out_file)
        img.read()
        # vérifie que l'image est bien une image avec des entiers, avec le
        # nombre de classes attendues
        self.assertLessEqual(len(np.unique(img.data)), 4)

    def test_classify_shp(self):
        """
        Test d'application d'une classification à un shapefile
        """
        out_file = os.path.join(DIR_TMP, 'export.shp')
        benShp = Benchmark(DIR_TMP, type='classification', 
                                algorithm='knn')
        benShp.load(os.path.join(DIR_DATA, 'echantillon.shp'), 
                         features=None, classe='ECH')
        benShp.load_data(os.path.join(DIR_DATA, 'data.shp'))
        benShp.training()
        benShp.run(True)
        
        # verifie que le nouveau shapefile est bien ecrit
        self.assertTrue(os.path.exists(out_file))
        shp = Shapefile(out_file)
        shp.read()
        # vérifie que l'image est bien une image avec des entiers, avec le
        # nombre de classes attendues
        self.assertLessEqual(len(np.unique(shp.data)), 3)

if __name__ == '__main__':
    unittest.main()

