# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 22:00:54 2015

@author: thomas_a
"""
from __future__ import print_function

import os
import ConfigParser
import unittest

from maplearn.test import cfg
from maplearn.app.config import Config


class TestConfig(unittest.TestCase):
    """ Tests unitaires de la classe Config
    """
    def test_missing_file(self):
        """ chargement d'une configuration à partir d'un fichier absent"""
        self.assertRaises(ConfigParser.Error, Config, 'missing_file.cfg')
    
    def test_check(self):
        """
        Vérifie la configuration du fichier de configuration actuelle
        """
        self.assertEqual(cfg.check(), 0)

    def test_check2(self):
        """
        Vérifie la configuration du fichier de configuration actuelle
        """
        __cfg = Config('config.cfg')
        self.assertEqual(__cfg.check(), 0)

    def test_get_legend(self):
        """
        Obtenir une legende (code/libelle des classes) à partir d'un fichier
        de configuration (avec une nomenclature contenant 7 codes)
        """
        __cfg = Config(os.path.join(cfg.dirs['bin'], 'examples', 'example2.cfg'))
        self.assertEqual(len(__cfg.entree['codes']), 7)

if __name__ == '__main__':
    unittest.main()
