# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:19:48 2016

@author: thomas_a
"""
import os
import unittest
import numpy as np
import random

from maplearn.test import DIR_DATA, DIR_TMP
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.ml.clustering import Clustering

class TestClustering(unittest.TestCase):
    """ Tests unitaires de la classe Clustering (classif non supervisée)
    """
    def setUp(self):
        loader = Loader('iris')
        # attention le jeu de donnes IRIS est entièrement connu (X et Y ont
        # mêmes dimensions et data = X)
        for f in os.listdir(DIR_TMP):
            os.remove(os.path.join(DIR_TMP, f))
        self.__data = PackData(X=loader.X, Y=loader.Y, data=loader.X)
        cls = Clustering(None)
        self.__algo = random.choice(cls.ALL_ALGOS.keys())

    def test_unknown_classifier(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Clustering, self.__data, 'inexistant')

    def test_unknowns_algorithms(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Clustering, self.__data,
                          [self.__algo, 'inexistant', 'nimporte'])
    def test_fit(self):
        """ Entrainement d'un clustering => _fitted = True
        """
        clf = Clustering(data=self.__data, algorithms=None, dirOut=DIR_TMP)
        clf.fit_1(self.__algo)
        self.assertTrue(clf._fitted)

    def test_predict(self):
        """ Prediction par clustering
        """
        clf = Clustering(data=self.__data, algorithms=None, dirOut=DIR_TMP)
        clf.fit_1(self.__algo)
        try:
            clf.predict_1(self.__algo)
        except Exception as e:
            self.fail("Echec %s lors prediction:\n%s" % (self.__algo, e))

    def test_algorithms(self):
        """ Prediction par clustering
        """
        clf = Clustering(data=self.__data, algorithms=None, dirOut=DIR_TMP)
        for algo in clf.algorithms:
            clf.fit_1(algo)
            try:
                clf.predict_1(algo)
            except Exception as e:
                self.fail("Echec algo : %s\n%s" % (algo, e))

    def test_image(self):
        """
        Clustering appliqué à une image
        """
        src = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        img = ImageGeo(src)
        img.read()
        data = PackData(data=img.img_2_data())
        clf = Clustering(data=data, algorithms=None,
                         dirOut=DIR_TMP)
        clf.fit_1('mkmeans')
        clf.predict_1('mkmeans')
        img.data_2_img(clf.result, True)
        out_file = os.path.join(DIR_TMP, 'clustering.tif')
        img.write(out_file)
        img = ImageGeo(out_file)
        img.read()
        # vérifie que l'image existe et qu'il s'agit bien d'une matrice entière
        self.assertTrue(os.path.exists(out_file))
        # vérifie que l'image esy bien une image avec des entiers
        self.assertLessEqual(len(np.unique(img.data)), 15)

if __name__ == '__main__':
    unittest.main()