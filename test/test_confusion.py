# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:05:01 2016

@author: thomas_a
"""
import unittest
import numpy as np

from maplearn.ml.confusion import Confusion

class TestConfusion(unittest.TestCase):
    """ Tests unitaires de la classe Confusion
    """
    def test_cm_kappa_eq1(self):
        """
        Une matrice de confusion calculéeà partir de la confrontation d'un
        vecteur avec lui-même => kappa = 1
        """
        y1 = np.random.randint(0, 200, 15)
        conf_mat = Confusion(y1, y1)
        conf_mat.calcul_matrice()
        conf_mat.calcul_kappa()
        self.assertEqual(conf_mat.kappa, 1)

    def test_kappa(self):
        """
        Le kappa est compris entre -1 et 1 : -1 <= k <= 1
        """
        y1 = np.random.randint(0, 200, 15)
        y2 = np.random.randint(0, 200, 15)
        conf_mat = Confusion(y1, y2)
        conf_mat.calcul_matrice()
        conf_mat.calcul_kappa()
        self.assertGreaterEqual(conf_mat.kappa, -1)
        self.assertLessEqual(conf_mat.kappa, 1)

if __name__ == '__main__':
    unittest.main()