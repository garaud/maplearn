# Mapping Learning #


Mapping Learning se veut un outil pour assister l'utilisateur dans une tache de cartographie automatisée (d'images issues de télédétection, notamment). Il donne accès à un grand nombre d'algorithmes issus du domaine du *Machine Learning*, ainsi que des moyens pour les appliquer à des jeux de données de différentes natures (tableau, image, données vectorielles).


## Fonctionnalités ##
* Nombreux algorithmes de classification supervisée et non supervisée (clustering) : svm, knn, Random Forest, lda...
* Emploi de méthode robuste : k-fold
* Optimisation d'algorithmes (recherche de paramètres optimaux)
* Plusieurs prétraitements : réductions de dimensions...
* Lecture/Ecriture d'une grande variété de formats de fichiers (texte, Excel(c), Tiff, Shapefile...)
* Rendu du résultat sous la forme d'une page web donnant un rendu synthétique et standardisé des résultats (matrices de confusion...)
* Comparaison du résultat des différents algorithmes
* Des conseils d'utilisation et d'éventuelles mises en garde de l'utilisateur sont données à chaque étape du traitement (développement en cours). 

## Développement ##

Mapping Learning est développé en Python et fait appel à des librairies Open Source. Les principales sont scikit-learn et mlpy pour les algorithmes de Machine Learning, osgeo (Gdal/Ogr) pour la manipulation des données géographiques.

Alban Thomas

![letg.jpeg](https://bitbucket.org/repo/5Rn74B/images/1800694338-letg.jpeg)![sgdn.jpeg](https://bitbucket.org/repo/5Rn74B/images/612087003-sgdn.jpeg)![ur2.png](https://bitbucket.org/repo/5Rn74B/images/2154561398-ur2.png)