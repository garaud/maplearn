# -*- coding: utf-8 -*-
"""
Chargement des données à partir de fichier ou de datasets exemples
Created on Wed Jun  4 12:53:48 2014

@author: thomas_a
"""
from __future__ import print_function

import os
import logging

import pandas as pd
import numpy as np
from sklearn import datasets

from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.filehandler.shapefile import Shapefile
from maplearn.filehandler.excel import Excel

logger = logging.getLogger('maplearn.'+__name__)
#TODO : créer une classe "csv" dans  filehandler qui devine quels parametres
#       ont été utilisés pour créer le fichier (séparateur...)
class Loader(object):
    """
    Chargement des données à partir de fichier ou de datasets exemples
    Created on Wed Jun  4 12:53:48 2014

    @author: thomas_a
    """
    __SOURCES = ['iris', 'digits', 'boston']  # liste des datasets dispo dans le code

    def __init__(self, source, **kwargs):
        self.src = {'type':'unknown', 'path':str(source).strip()}
        self.__df = None
        self.X = None
        self.Y = None
        self.aData = None
        self.matrix = None # pour les données matricielles
        self.features = None
        self.nomenclature = None
        self.__NA = None

        if 'codes' in kwargs:
            self.nomenclature = kwargs['codes']

        if 'NA' in kwargs:
            self.__NA = kwargs['NA']

        if self.src['path'] in self.__SOURCES:
            self.__from_dataset()
        else:
            self.__from_file()
            self.run(**kwargs)

    def __from_dataset(self):
        """
        Chargement de données à partir d'un dataset de scikit (pour test)
        """
        if self.src['path'] not in self.__SOURCES:
            raise IOError("Source inconnue : %s" % self.src['path'])

        logger.info('Loading %s dataset...', self.src['path'])
        ds_load = eval('datasets.load_%s()' % self.src['path'])

        self.X = ds_load.data
        self.Y = ds_load.target
        logger.info('Dataset %s loaded : %i data * %i features',
                     self.src['path'], self.X.shape[0], self.X.shape[1])

        # Création de la nomenclature (code classe : libelle de classe)
        if 'target_names' in ds_load.keys():
            libelles = [str(n) for n in ds_load.target_names]
            self.nomenclature = dict(zip(np.unique(ds_load.target), libelles))
        else:
            logger.warning('%s dataset has no target names', self.src['path'])        
        
        if 'feature_names' in ds_load.keys():
            self.features = list(ds_load.feature_names)
        self.src['type'] = 'dataset'

    def __from_file(self):
        """
        Chargement de données à partir d'un fichier
        """
        logger.info('Lecture a partir du fichier : %s', self.src['path'])
        s_ext = os.path.splitext(self.src['path'])[1].lower()
        if s_ext in ['.xls', '.xlsx']:
            self.src['type'] = 'Excel'
            ofi_src = Excel(self.src['path'])
            self.__df = ofi_src.read()
            ofi_src = None
        elif s_ext == '.csv':
            self.src['type'] = 'Csv'
            self.__df = pd.read_csv(self.src['path'], sep=None)
        elif s_ext == '.shp':
            self.src['type'] = 'Shapefile'
            ofi_src = Shapefile(self.src['path'])
            self.__df = ofi_src.read()
            ofi_src = None
        elif s_ext == '.tif' or s_ext == '':
            self.src['type'] = 'ImageGeo'
            ofi_src = ImageGeo(self.src['path'])
            ofi_src.read()
            self.matrix = ofi_src.data
            self.__df = ofi_src.img_2_data()
            ofi_src = None
        else:
            raise IOError("Type de fichier inconnu : %s" % self.src['path'])
        
        self.features = list(self.__df.columns)

    def run(self, **kwargs):
        """
        Recupere X et Y a partir du dataframe
            - features : liste des features à charger
            - classe : colonne avec les classes
            - classe_id : colonne avec les ID des classes
        """
        if 'classe_id' in kwargs:
            classe_id = kwargs['classe_id']
        else:
            classe_id = None
        if 'classe' in kwargs:
            classe = kwargs['classe']
        else:
            classe = None

        # NOMENCLATURE : recuperation des codes et libelles des classes
        logger.debug('Constitution nomenclature [classe_id:%s - classe:%s]...',
                     classe_id, classe)
        if self.nomenclature is None:
            if classe is not None and classe_id is not None:
                codes = self.__df[[classe_id, classe]].drop_duplicates()
                codes[classe] = codes[classe].astype(str)
                dct_codes = codes.set_index(classe_id)[classe].to_dict()
            elif classe is not None and classe_id is None:
                codes = pd.DataFrame(data=self.__df[[classe]].drop_duplicates())
                codes[classe] = codes[classe].astype(str)
                codes = codes.sort(classe)
                codes['classe_id'] = range(1, codes.shape[0]+1)
                dct_codes = codes.set_index('classe_id')[classe].to_dict()
            elif classe is None and classe_id is not None:
                codes = self.__df[[classe_id]].drop_duplicates()
                pd.DataFrame(data=codes)
                codes = codes[codes.notnull()]
                codes['classe'] = codes[classe_id].astype(str)
                dct_codes = codes.set_index(classe_id)['classe'].to_dict()
            else:
                dct_codes = None
            self.nomenclature = dct_codes

        # JEUX DE DONNEES
        features = kwargs['features'] if 'features' in kwargs else None
        if features is None:
            features = self.features

        # selection des colonnes a conserver
        if classe_id is not None and classe_id in features:
            features.remove(classe_id)
        if classe is not None and classe in features:
            features.remove(classe)

        # cas 1 : pas de classes désignées => DATA
        if classe is None and classe_id is None:
            #self.aData = np.array(self.__df[features]).astype(np.float)
            self.aData = np.array(self.__df[features])
        # cas 2 : creation de X+Y (& eventuellement data)
        else:
            # creation de X
            self.X = np.array(self.__df[features]).astype(np.float)

            # Creation de Y
            if classe_id is not None:
                self.Y = np.array(self.__df[classe_id]).astype(np.int)
            else:
                self.Y = np.zeros(self.__df.shape[0], dtype=np.int)
                y = self.__df[classe].values
                for c in np.unique(y):
                    self.Y[np.where(y == c)] = [k for (k, v) \
                                    in self.nomenclature.iteritems() if v == c]

            # Exclusion des NA (si NA dans X)
            col = [c for c in [classe, classe_id] if c is not None]
            if len(col) > 0:
                self._select(col[0])
            logger.info('Data loaded: %i data * %i features',
                         self.X.shape[0], self.X.shape[1])

        self.features = features

    def _select(self, col):
        """
        Selection des donnees avec des echantillons
        Distinction entre :
            -X & Y, individus avec des echantillons
            -aData: tous les individus
        """
        idx = self.__df[col].notnull().values
        if len(idx) > 0:
            self.aData = np.copy(self.X)
            self.X = self.X[idx, :]
            self.Y = self.Y[idx]

    def __str__(self):
        return '\nSOURCE : %s' % self.src['path']
