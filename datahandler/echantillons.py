# -*- coding: utf-8 -*-
"""
Scripts sur la manipulation des echantillons
Created on Thu Jan 24 11:46:15 2013

@author: thomas_a
"""
from __future__ import print_function
from collections import Counter
import logging

import numpy as np

logger = logging.getLogger('maplearn.'+__name__)

class Echantillon(object):
    """
    Echantillons utilisées dans PackData
    - dct_codes contient les ids des classes des échantillons, associés
      à leurs libellés
    """
    def __init__(self, Y, codes=None):
        self.__y = None
        self.summary = None
        self.dct_codes = codes
        self.Y = Y

    def count(self):
        """ compte les fréquences associées à chaque classe
        """
        ukeys = np.unique(self.__y)
        bins = ukeys.searchsorted(self.__y)
        l_nb = np.bincount(bins)
        self.summary = dict(zip(ukeys, l_nb))

    def convert(self):
        """
        Conversion entre codes
        """
        if self.dct_codes is None:
            logger.error('Conversion impossible des codes des échantillons')
        else:
            self.__y = np.array([self.dct_codes[i] for i in self.__y])

    def libelle2code(self):
        """
        Conversion des libelles des classes en codes correspondants
        """
        if  self.__y.dtype == 'S1':
            self.convert()
        else:
            logger.info("Y déjà sous forme d'un code numérique")

    @property
    def Y(self):
        """
        Renvoie le vecteur d'échantillons
        """
        return self.__y

    @Y.setter
    def Y(self, Y):
        """
        Affecte les échantillons en vérifiant leur correspondance avec la
        nomenclature
        """
        if Y.ndim != 1:
            str_msg = "Y doit avoir une dimension (%i vus)" % Y.ndim
            logger.critical(str_msg)
            raise IndexError(str_msg)

        # gestion de la nomenclature en fonction des codes donnés en entrée
        # et des valeurs observées dans Y
        if not self.dct_codes is None:
            codes_y = list(np.unique(Y))
            tmp = [c for c in codes_y if c not in self.dct_codes.keys()]
            # creation de nouveaux codes si manquent dans nomenclature
            if len(tmp) > 0:
                logger.info('Les codes suivants vont être ajoutés :')
                for code in tmp:
                    self.dct_codes[code] = str(code)
                    logger.info('Nouveau code : %i (%s)', code,
                                 self.dct_codes[code])

            if len(self.dct_codes) > len(codes_y):
                logger.warning('Code(s) sans échantillon')
        else:
            self.dct_codes = dict(zip(np.unique(Y),
                                      [str(i) for i in np.unique(Y)]))

        # Si plusieurs classes ont le même code => agréger ces classes
        counts = Counter(self.dct_codes.values())
        duplic = [k for k in counts if counts[k] > 1]
        if len(duplic) > 0:
            logger.debug('%i code(s) à recoder', len(duplic))
            for libelle in duplic:
                codes = [k for k in self.dct_codes \
                         if self.dct_codes[k] == libelle]
                for code in codes[1:]:
                    Y[Y == code] = codes[0]
                    self.dct_codes.pop(code)

                print('\t* [%s] => %i (%s)' % (','.join([str(code) for code \
                      in codes[1:]]), codes[0], self.dct_codes[codes[0]]))
            logger.info('%i code(s) recodé(s)', len(duplic))

        # Affectation de Y et compte des echantillons par classe
        self.__y = Y
        self.count()

    def __str__(self):

        ukeys = np.unique(self.__y)
        if len(ukeys) < 1:
            return "\n### Echantillons ### \n* Aucun echantillon disponible"

        str_msg = "\n### Description des echantillons ###"
        str_msg += "\n%i Echantillons - %i classes : \n  " % (len(self.__y),
                                                              len(ukeys))
        n_congalton = 0
        for i in sorted(self.summary.keys()):
            str_msg += "\n* %s (%i) : %i echantillons " % (self.dct_codes[i],
                                                           i, self.summary[i])
            if self.summary[i] < 50:
                n_congalton += 1
                str_msg += "(1)  "
        str_msg += "  \n  "
        # Affichage des classes avec insuffisamment d'échantillons
        if n_congalton > 0:
            str_msg += "\n **WARNING :**\n"
            str_msg += "*(1) %i classe(s) avec trop peu d'echantillons " \
                       % n_congalton
            str_msg += "[regle empirique de Congalton : >= 50 echantillons]*\n"

        return str_msg
