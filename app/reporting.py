# -*- coding: utf-8 -*-
"""
Script pour générer du html à partir d'un fichier texte

Created on Wed Jul  8 21:35:23 2015

@author: thomas_a
"""
import sys
import markdown
import webbrowser

class ReportWriter(object):
    """
    Capture et retranscrit tous les print() en langage Markdown pour pouvoir
    générer un rapport standardisé
    """
    def __init__(self, filename):
        self.__ori_stdout = sys.stdout
        self.stdout = sys.stdout
        self.__file = filename
        self.txtfile = None
        self.htmlfile = None
        self.open_(filename)

    def open_(self, filename, mode='w'):
        """
        Ouvre 2 fichiers (un en texte et l'autre en html)
        """
        self.txtfile = open(filename + '.txt', mode)
        self.htmlfile = open(filename + '.html', mode)
        self.__file = filename

    def write(self, text):
        """
        Ecrit ce qui est affiché à l'écran :
        - en texte
        - au format HTML
        """
        #self.stdout.write(text)
        self.txtfile.write(text)
        self.htmlfile.write(markdown.markdown(text, \
                            extensions=['markdown.extensions.tables',]))

    def close(self):
        #self.stdout.close()
        self.txtfile.close()
        self.htmlfile.close()
        self.txtfile = None
        self.htmlfile = None
        sys.stdout = self.__ori_stdout
        webbrowser.open("file://%s.html" % self.__file)

    def flush(self):
        """
        Vide tous les flux en cours
        """
        try:
            self.stdout.flush()
            self.txtfile.flush()
            self.htmlfile.flush()
        except ValueError:
            self.open_(self.__file, 'a')

def str_table(header, size=20, **kwargs):
    """
    renvoie un tableau sous forme de texte un tableau
    """
    n_cols = len(header)
    n_rows = len(kwargs[kwargs.keys()[0]])

    # header
    str_msg = '\n|%s|\n' % '|'.join([str_extend(txt, size=size)
                                     for txt in header])
    str_msg += '|%s|\n' % '|'.join(['-'*size,]*n_cols)
    # data
    data = [kwargs[c] for c in header]
    i = 0
    while i < n_rows:
        line = [str(v[i]) for v in data]
        str_msg += '|%s|\n' % '|'.join([str_extend(txt, size=size)
                                        for txt in line])
        i += 1
    return str_msg

def str_extend(txt, size=20):
    """
    Etend une chaine de caractères au nombre de caractères demandés (size)
    en ajoutant des ' ' à la fin
    """
    return txt+' '*(size-len(txt))
